<?php
namespace Magenest\Movie\Controller\Adminhtml\Director;

use Magento\Backend\App\Action;

class Save extends Action
{

    protected $_model;


    public function __construct(
        Action\Context $context,
        \Magenest\Movie\Model\Director $model
    ) {
        parent::__construct($context);
        $this->_model = $model;
    }


    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Movie::director_save');
    }


    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            $model = $this->_model;

            $id = $this->getRequest()->getParam('director_id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);



            try {
                $model->save();
                $movieid = $model->getData();
                $idm = $movieid['director_id'];


                $this->messageManager->addSuccess(__('Director saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getDirectorId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e->getMessage());
            }


            $this->_getSession()->setFormData($data);

            return $resultRedirect->setPath('*/*/edit', ['director_id' => $this->getRequest()->getParam('director_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
