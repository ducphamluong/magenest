<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var \Maxime\Jobs\Model\Department
     */
    protected $_model;

    /**
     * @param Action\Context $context
     * @param \Maxime\Jobs\Model\Department $model
     */
    public function __construct(
        Action\Context $context,
        \Magenest\Movie\Model\Movie $model
    ) {
        parent::__construct($context);
        $this->_model = $model;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Movie::movie_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Maxime\Jobs\Model\Department $model */
            $model = $this->_model;

            $id = $this->getRequest()->getParam('movie_id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);



            try {
                $model->save();
                $movieid = $model->getData();
                $idm = $movieid['movie_id'];
//                var_dump($idm);
//                var_dump($movieid);
                $this->_eventManager->dispatch(
                    'change_save',
                    ['movie' => $model, 'request' => $this->getRequest(), 'idm'=>$idm]
                );

                $this->messageManager->addSuccess(__('Department saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getMovieId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e->getMessage());
            }


            $this->_getSession()->setFormData($data);

            return $resultRedirect->setPath('*/*/edit', ['movie_id' => $this->getRequest()->getParam('movie_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}

///**
// * Webkul Grid Row Save Controller
// *
// * @category    Webkul
// * @package     Webkul_Grid
// * @author      Webkul Software Private Limited
// *
// */
//namespace Magenest\Movie\Controller\Adminhtml\Movie;
//
//class Save extends \Magento\Backend\App\Action
//{
//    /**
//     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
//     * @SuppressWarnings(PHPMD.NPathComplexity)
//     */
//    public function execute()
//    {
//        $data = $this->getRequest()->getPostValue();
//        if (!$data) {
//            $this->_redirect('magenest_movie/movie/addrow');
//            return;
//        }
//        try {
//            $rowData = $this->_objectManager->create('Magenest\Movie\Model\Movie');
//            $rowData->setData($data);
//            if (isset($data['movie_id'])) {
//                $rowData->setEntityId($data['movie_id']);
//            }
//            $rowData->save();
//            $this->messageManager->addSuccess(__('Row data has been successfully saved.'));
//        } catch (Exception $e) {
//            $this->messageManager->addError(__($e->getMessage()));
//        }
//        $this->_redirect('magenest_movie/movie/index');
//    }
//
//    /**
//     * Check Category Map permission.
//     *
//     * @return bool
//     */
////    protected function _isAllowed()
////    {
////        return $this->_authorization->isAllowed('Webkul_Auction::add_auction');
////    }
//}
