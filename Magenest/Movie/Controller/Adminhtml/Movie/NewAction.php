<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;
use Magento\Backend\App\Action;
use Magenest\Movie\Model\Movie as Movie;

class NewAction extends \Magento\Backend\App\Action
{
    /**
     * Edit A movie Page
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();

        $movieDatas = $this->getRequest()->getParam('movie');
        if (is_array($movieDatas)) {
            $movie = $this->_objectManager->create(movie::class);
            $movie->setData($movieDatas)->save();
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }
    }
}
