<?php
namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magento\Backend\App\Action;

class Save extends Action
{

    protected $_model;


    public function __construct(
        Action\Context $context,
        \Magenest\Movie\Model\Actor $model
    ) {
        parent::__construct($context);
        $this->_model = $model;
    }


    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Movie::actor_save');
    }


    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
//        var_dump($data);
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Maxime\Jobs\Model\Department $model */
            $model = $this->_model;

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }
//            var_dump($model);
//            echo "<hr/><br>";
            $model->setData($data);
//            var_dump($model);
//            echo "<hr/>";
//
//            $this->_eventManager->dispatch(
//                'change_save',
//                ['actor' => $model, 'request' => $this->getRequest()]
//            );
//            var_dump($this->getRequest());


            try {
                $model->save();
                $this->messageManager->addSuccess(__('Actor saved'));

                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the actor'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['actor_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
