<?php
namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magento\Backend\App\Action;

class Edit extends Action
{

    protected $_coreRegistry = null;


    protected $_resultPageFactory;

    protected $_model;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magenest\Movie\Model\Actor $model
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->_model = $model;
        parent::__construct($context);
    }


    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Movie::actor_save');
    }


    protected function _initAction()
    {

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Movie::actor')
            ->addBreadcrumb(__('Actor'), __('Actor'))
            ->addBreadcrumb(__('Manage Actor'), __('Manage Actor'));
        return $resultPage;
    }


    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_model;

        // If you have got an id, it's edition
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This Actor not exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->_coreRegistry->register('actor', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Actor') : __('New Actor'),
            $id ? __('Edit Actor') : __('New Actor')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Actors'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getName() : __('New Actor'));

        return $resultPage;
    }
}
