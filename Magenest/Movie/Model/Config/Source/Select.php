<?php
namespace Magenest\Movie\Model\Config\Source;
class Select implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => '2',
                'label' => __('Don\'t Show')
            ],
            [
                'value' => '1',
                'label' => __('Show')
            ]
        ];
    }
}
