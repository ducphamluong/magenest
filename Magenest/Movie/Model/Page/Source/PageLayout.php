<?php
// 6. 3 advanced backend
namespace Magenest\Movie\Model\Page\Source;
use Magento\Framework\Option\ArrayInterface;

class PageLayout implements ArrayInterface
{
    protected $options;
    protected $directorFactory;
    public function __construct(
        \Magenest\Movie\Model\ResourceModel\Director\CollectionFactory $directorFactory
    ) {
        $this->directorFactory = $directorFactory;
    }
    public function toOptionArray()
    {
        $director = $this->directorFactory->create();
//        var_dump($director->addFieldToSelect( array('director_id','name') ));

        $data = $director->addFieldToSelect( array('director_id','name') );
        $options[] = array('label' => 'Please select', 'value' => '2'  );
        if( $data->getSize() ){
            foreach ( $data  as $seller) {
                $options[] = array(
                    'label' => ucfirst( $seller->getData('name') ) ,
                    'value' => $seller->getData('director_id')
                );
            }
        }
        return $options;
    }
}
