<?php
namespace Magenest\Movie\Model;
class Movie extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'magenest_movie'; //database
    const MOVIE_ID = 'MOVIE_ID';
    const NAME = 'NAME';

    protected $_cacheTag = 'magenest_movie';

    protected $_eventPrefix = 'magenest_movie';

    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\ResourceModel\Movie');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
    public function getMovieId()
    {
        return $this->getData(self::MOVIE_ID);
    }
//
//    /**
//     * Set EntityId.
//     */
    public function setMovieId($entityId)
    {
        return $this->setData(self::MOVIE_ID, $entityId);
    }
//
//    /**
//     * Get Title.
//     *
//     * @return varchar
//     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set Title.
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
//
//    /**
//     * Get getContent.
//     *
//     * @return varchar
//     */
//    public function getContent()
//    {
//        return $this->getData(self::DESCRIPTION);
//    }
//
//    /**
//     * Set Content.
//     */
//    public function setContent($content)
//    {
//        return $this->setData(self::DESCRIPTION, $content);
//    }
//
//    /**
//     * Get PublishDate.
//     *
//     * @return varchar
//     */
//    public function getPublishDate()
//    {
//        return $this->getData(self::PUBLISH_DATE);
//    }
//
//    /**
//     * Set PublishDate.
//     */
//    public function setPublishDate($publishDate)
//    {
//        return $this->setData(self::PUBLISH_DATE, $publishDate);
//    }
//
//    /**
//     * Get IsActive.
//     *
//     * @return varchar
//     */
//    public function getIsActive()
//    {
//        return $this->getData(self::IS_ACTIVE);
//    }
//
//    /**
//     * Set IsActive.
//     */
//    public function setIsActive($isActive)
//    {
//        return $this->setData(self::IS_ACTIVE, $isActive);
//    }
//
//    /**
//     * Get UpdateTime.
//     *
//     * @return varchar
//     */
//    public function getUpdateTime()
//    {
//        return $this->getData(self::UPDATE_TIME);
//    }
//
//    /**
//     * Set UpdateTime.
//     */
//    public function setUpdateTime($updateTime)
//    {
//        return $this->setData(self::UPDATE_TIME, $updateTime);
//    }
//
//    /**
//     * Get CreatedAt.
//     *
//     * @return varchar
//     */
//    public function getCreatedAt()
//    {
//        return $this->getData(self::CREATED_AT);
//    }
//
//    /**
//     * Set CreatedAt.
//     */
//    public function setCreatedAt($createdAt)
//    {
//        return $this->setData(self::CREATED_AT, $createdAt);
//    }
//    public function getRating()
//    {
//        return $this->getData(self::RATING);
//    }
//    public function setRating($rating)
//    {
//        return $this->setData(self::RATING, $rating);
//    }

}
