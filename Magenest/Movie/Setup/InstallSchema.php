<?php

namespace Magenest\Movie\Setup;
use Faker\Guesser\Name;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Create table 'magenest_movie'
         */
        if (!$setup->tableExists('magenest_movie')) {
            $table = $setup->getConnection()
                ->newTable($setup->getTable('magenest_movie'))
                ->addColumn(
                    'movie_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Movie ID'
                )
                ->addColumn(
                    'name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255,
                    ['nullable' => false, 'unsigned'=>true],
                    'Name'
                )
                ->addColumn(
                    'description',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255,
                    ['nullable' => false],
                    'Description'
                )
                ->addColumn(
                    'rating',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null,
                    ['nullable' => false, 'unsigned' => true],
                    'Rating'
                )
                ->addColumn(
                    'director_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null,
                    ['nullable' => false, 'unsigned' => true],
                    'Director ID'
                )
                ->setComment("Movie table");
            $setup->getConnection()->createTable($table);
        }
        if (!$setup->tableExists('magenest_director')) {
            $table = $setup->getConnection()
                ->newTable($setup->getTable('magenest_director'))
                ->addColumn(
                    'director_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Director ID'
                )
                ->addColumn(
                    'name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255,
                    ['nullable' => false],
                    'Name'
                )->setComment("Director table");
            $setup->getConnection()->createTable($table);
        }
        if (!$setup->tableExists('magenest_actor')) {
            $table = $setup->getConnection()
                ->newTable($setup->getTable('magenest_actor'))
                ->addColumn(
                    'actor_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'actor ID'
                )
                ->addColumn(
                    'name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255,
                    ['nullable' => false],
                    'Name'
                )->setComment("Actor table");
            $setup->getConnection()->createTable($table);
        }
        if (!$setup->tableExists('magenest_movie_actor')) {
            $table = $setup->getConnection()
                ->newTable($setup->getTable('magenest_movie_actor'))
                ->addColumn(
                    'movie_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null,
                    ['unsigned' => true, 'nullable' => false],
                    'Movie ID'
                )
                ->addColumn(
                    'actor_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null,
                    ['unsigned' => true, 'nullable' => false],
                    'Actor ID'
                )->setComment("Movie Actor table");
            $setup->getConnection()->createTable($table);
        }

        $setup->getConnection()->addForeignKey(
            $setup->getFkName('magenest_movie', 'director_id',
                $setup->getTable('magenest_director'), 'director_id'),
//            $setup->getFkName('magenest_movie_actor', 'movie_id', $setup->getTable('magenest_movie'), 'movie_id'),
//            $setup->getFkName('magenest_movie_actor', 'director_id', $setup->getTable('magenest_director'), 'director_id'),
            $setup->getTable('magenest_movie'),
            'director_id',
            $setup->getTable('magenest_director'), 'director_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        $setup->getConnection()->addForeignKey(
            $setup->getFkName('magenest_movie_actor', 'movie_id',
                $setup->getTable('magenest_movie'), 'movie_id'),
            $setup->getTable('magenest_movie_actor'),
            'movie_id',
            $setup->getTable('magenest_movie'), 'movie_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $setup->getConnection()->addForeignKey(
            $setup->getFkName('magenest_movie_actor', 'actor_id',
                $setup->getTable('magenest_actor'), 'actor_id'),
            $setup->getTable('magenest_movie_actor'),
            'actor_id',
            $setup->getTable('magenest_actor'), 'actor_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
    }
}
