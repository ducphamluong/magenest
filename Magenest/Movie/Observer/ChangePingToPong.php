<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class ChangePingToPong implements ObserverInterface
{
    const XML_PATH_FAQ_URL = 'movie/moviepage/text_field';
    private $request;
    private $configWriter;

    public function __construct(
        RequestInterface $request,
        WriterInterface $configWriter
    ) {
        $this->request = $request;
        $this->configWriter = $configWriter;

    }
    public function execute(EventObserver $observer)
    {
        $meetParams = $this->request->getParam('groups');
        $name = $meetParams['moviepage']['fields']['text_field']['value'];
        if($name =='Ping') {
            $this->configWriter->save('movie/moviepage/text_field', 'Pong');
}


        //to print thatn value in system.log
//        \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->info($name);
    }
}
