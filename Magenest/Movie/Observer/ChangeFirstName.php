<?php
namespace Magenest\Movie\Observer;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
class ChangeFirstName implements ObserverInterface
{
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface)
{

    $this->_request = $context->getRequest();
    $this->_customerRepositoryInterface = $customerRepositoryInterface;
}

public function execute(EventObserver $observer)
{
//    var_dump($this->_request->getPost());
    $customer = $observer->getEvent()->getCustomer();
    $firstName = $customer->getFirstName();
    echo "First Name Input: ".$firstName;
    echo "<br>";
    $customer->setFirstName('Magenest');
    $firstName = $customer->getFirstName();
    echo "\$customer->getFirstName(): ".$firstName;
    $this->_customerRepositoryInterface->save($customer);
    echo "<br>";
    echo "Change seccess !";
    exit();
}
}
