<?php
namespace Magenest\Movie\Observer;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Event\ObserverInterface;
class ChangeRatingMovie implements ObserverInterface
{
    protected $_customerRepositoryInterface;
    protected $_request;
    protected $_pageFactory;
    protected $_movieFactory;

    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Framework\View\Result\PageFactory $pageFactory,
                                \Magenest\Movie\Model\Movie $movieFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface)
    {
        $this->_pageFactory = $pageFactory;
        $this->_request = $context->getRequest();
        $this->_movieFactory = $movieFactory;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
//        return parent::__construct($context);
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

//        $movie = $this->_movieFactory->create();
//        $collection = $movie->getCollection();
//        $data = $collection->getData();
        $movie = $observer->getEvent()->getData();
        $mid = $movie['idm'];
//        var_dump($mid);
//        $movieFactory->load(1);
//        var_dump($movieFactory);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $mv = $objectManager->create('Magenest\Movie\Model\Movie');
//        var_dump($mv);
        $data =  $mv->load($mid)->getData();
        $data['rating']= "0";
//        var_dump($data);
        $mv->load($mid)->setData($data);
        $mv->save();
    }

}
