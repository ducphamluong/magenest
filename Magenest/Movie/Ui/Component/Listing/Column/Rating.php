<?php
//Change view rating movie page
namespace Magenest\Movie\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\Api\SearchCriteriaBuilder;

class Rating extends Column
{
    protected $customerFactory;
    protected $_searchCriteria;


    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        SearchCriteriaBuilder $criteria,
        array $components = [], array $data = [])
    {
        $this->_searchCriteria = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {

            foreach ($dataSource['data']['items'] as & $item) {

                switch ($item[$this->getData('name')]) {
                    case "0" :
                        $item['rating'] = 'Zero';
                        break;
                    case "1" :
                        $item['rating'] = '<strong style="color:red">★</strong>';
                        break;
                    case "2" :
                        $item['rating'] = '<strong style="color:red">★★</strong>';
                        break;
                    case "3" :
                        $item['rating'] = '<strong style="color:red">★★★</strong>';
                        break;
                    case "4" :
                        $item['rating'] = '<strong style="color:red">★★★★</strong>';
                        break;
                    case "5" :
                        $item['rating'] = '<strong style="color:red">★★★★★</strong>';
                        break;
                    default:
                        $item['rating'] = '<strong style="color:red">★★★★★</strong>';
                        break;
                }
            }
        }
        return $dataSource;
    }
}

