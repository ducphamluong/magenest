<?php
// 6.2 advanced
namespace Magenest\Movie\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\Api\SearchCriteriaBuilder;

class OddEven extends Column
{
    protected $customerFactory;
    protected $_searchCriteria;


    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        SearchCriteriaBuilder $criteria,
        array $components = [], array $data = [])
    {
        $this->_searchCriteria = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {

            foreach ($dataSource['data']['items'] as & $item) {
//                if($item['entity_id']%2==0)
                $order  = $item["entity_id"];
//                var_dump($order);
                if(($order%2)==0){
                    $message = "<b id='even'>SUCCESS</b>";
                }
                else {
                    $message = "<b id='odd'>ERROR</b>";
                }
                $item[$this->getData('name')] = $message;

            }
        }
//        var_dump($dataSource['data']['items']);
//        die();
        return $dataSource;
    }
}

