<?php
namespace Magenest\Movie\Block;
use Magento\Framework\View\Element\Template;
class Index extends Template
{
    private $_productCollectionFactory;
    protected $_resource;
    protected $_movieFactory;
    public function __construct(
        Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
        $productCollectionFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magenest\Movie\Model\MovieFactory $movieFactory,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_resource = $resource;
        $this->_movieFactory = $movieFactory;
    }
    public function getQueryData()
    {
//        $query = $this->connection->fetchAll("SELECT m.movie_id,m.name,m.description,m.rating,m.director_id, a.name as aname, d.name as dname FROM magenest_movie as m inner join magenest_movie_actor as ma on m.movie_id=ma.movie_id inner join magenest_actor as a on ma.actor_id=a.actor_id inner join magenest_director as d on m.director_id = d.director_id order by m.movie_id");
//        return $query;
        $collection = $this->_movieFactory->create()->getCollection();
        $table_ma = $this->_resource->getTableName('magenest_movie_actor');
        $table_a = $this->_resource->getTableName('magenest_actor');
        $table_d = $this->_resource->getTableName('magenest_director');
        $collection->getSelect()
            ->joinLeft(array('ma' => $table_ma),
                'main_table.movie_id = ma.movie_id',
                [
                    "movie_id" => "main_table.movie_id"
                ])
            ->joinLeft(array('a' => $table_a),
                'ma.actor_id = a.actor_id',
                [
                    "aname" => "a.name"
                ])
            ->joinLeft(array('d' => $table_d),
                'main_table.director_id = d.director_id',
                [
                    "dname" => "d.name"
                ]);
        return $collection;
//        echo $collection->getSelect();die;
    }
}

