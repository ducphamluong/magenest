<?php
namespace Magenest\Movie\Block\Adminhtml;

class Grid extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        $this->_controller = 'adminhtml_grid';
        $this->_blockGroup = 'Magenest_Movie';
        $this->_headerText = __('Manage Movie');
//        $this->_addButtonLabel = __('New Movie');
        parent::_construct();


//        $this->removeButton('add');
// for remove btn
//
        $this->buttonList->remove('add');
//        $this->buttonList->remove('back');
//        $this->buttonList->remove('reset');
        $this->buttonList->add(
            'add',
            [
                'label' => __('New Movie'),
                'class' => 'primary',
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/edit') . '\')'
            ]
        );


    }
}
