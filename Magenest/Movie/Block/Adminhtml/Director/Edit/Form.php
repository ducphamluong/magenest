<?php

namespace Magenest\Movie\Block\Adminhtml\Director\Edit;

use \Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic
{


    protected $_systemStore;


    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }


    protected function _construct()
    {
        parent::_construct();
        $this->setId('director_form');
        $this->setTitle(__('Director Information'));
    }


    protected function _prepareForm()
    {

        $model = $this->_coreRegistry->registry('director');


        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('director_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getDirectorId()) {
            $fieldset->addField('director_id', 'hidden', ['name' => 'director_id']);
        }
        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Name'),
//                'id' => 'name',
//                'title' => __('Title'),
                'class' => 'required-entry',

            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}

