<?php

namespace Magenest\Movie\Block\Adminhtml\Director;

use Magento\Backend\Block\Widget\Form\Container;

class Addnew extends Container
{
    protected $_coreRegistry = null;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }


    protected function _construct()
    {
        $this->_objectId = 'director_id';
        $this->_blockGroup = 'Magenest_Movie';
        $this->_controller = 'adminhtml_director';

        parent::_construct();

        if ($this->_isAllowedAction('Magenest_Movie::director')) {
            $this->buttonList->update('save', 'label', __('Save Director'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        } else {
            $this->buttonList->remove('save');
        }

    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }


    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', ['_current' => true, 'back' => 'addnew', 'active_tab' => '']);
    }
}
