<?php
namespace Magenest\Movie\Block\Adminhtml;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Count extends Field
{

    protected $_template = 'Magenest_Movie::system/config/count.phtml';
    protected $movie;

    public function __construct(
        Context $context,
        \Magenest\Movie\Model\Movie $movie,
        array $data = []
    )
    {
        $this->movie = $movie;
        parent::__construct($context, $data);

    }
    public function count()
    {
        $movieCollection = $this->movie->getCollection();
        return $movieCollection;
//        $query = $this->connection->fetchAll("SELECT COUNT(*) as count from magenest_movie");
//        return $query;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }


}

