<?php

namespace Magenest\Movie\Block\Adminhtml\Movie\Edit;

use \Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('movie_form');
        $this->setTitle(__('Movie Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Maxime\Jobs\Model\Department $model */
        $model = $this->_coreRegistry->registry('movie_movie');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('movie_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getMovieId()) {
            $fieldset->addField('movie_id', 'hidden', ['name' => 'movie_id']);
        }
        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Name'),
//                'id' => 'name',
//                'title' => __('Title'),
                'class' => 'required-entry',

            ]
        );
        $fieldset->addField(
            'description',
            'text',
            [
                'name' => 'description',
                'label' => __('Description'),
//                'id' => 'name',
//                'title' => __('Title'),
                'class' => 'required-entry',

            ]
        );
        $fieldset->addField(
            'rating',
            'text',
            [
                'name' => 'rating',
                'label' => __('Rating'),
//                'id' => 'name',
//                'title' => __('Title'),
                'class' => 'required-entry',

            ]
        );
        $fieldset->addField(
            'director_id',
            'text',
            [
                'name' => 'director_id',
                'label' => __('Director_ID'),
//                'id' => 'name',
//                'title' => __('Title'),
                'class' => 'required-entry',

            ]
        );

//
//        $fieldset->addField(
//            'name',
//            'text',
//            ['name' => 'name', 'label' => __('Department Name'), 'title' => __('Department Name'), 'required' => true]
//        );
//
//        $fieldset->addField(
//            'description',
//            'textarea',
//            ['name' => 'description', 'label' => __('Department Description'), 'title' => __('Department Description'), 'required' => true]
//        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}

//class Form extends \Magento\Backend\Block\Widget\Form\Generic
//{
//
//    public function __construct(
//        \Magento\Backend\Block\Template\Context $context,
//        \Magento\Framework\Registry $registry,
//        \Magento\Framework\Data\FormFactory $formFactory,
//        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
////        \Magenest\Movie\Model\Status $options,
//        array $data = []
//    )
//    {
////        $this->_options = $options;
//        $this->_wysiwygConfig = $wysiwygConfig;
//        parent::__construct($context, $registry, $formFactory, $data);
//    }
//
//    /**
//     * Prepare form.
//     *
//     * @return $this
//     */
//    protected function _prepareForm()
//    {
////        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
//        $model = $this->_coreRegistry->registry('row_data');
//        $form = $this->_formFactory->create(
//                 ['data' => [
//                'id' => 'form',
//                'enctype' => 'multipart/form-data',
//                'action' => $this->getData('action'),
//                'method' => 'post'
//            ]
//            ]
//        );
//
//        $form->setHtmlIdPrefix('movie_');
//        if ($model->getId()) {
//            $fieldset = $form->addFieldset(
//                'base_fieldset',
//                ['legend' => __('Edit Row Data'), 'class' => 'fieldset-wide']
//            );
//            $fieldset->addField('movie_id', 'hidden', ['name' => 'movie_id']);
//        } else {
//            $fieldset = $form->addFieldset(
//                'base_fieldset',
//                ['legend' => __('Add Row Data'), 'class' => 'fieldset-wide']
//            );
//        }
//
//        $fieldset->addField(
//            'name',
//            'text',
//            [
//                'name' => 'name',
//                'label' => __('Name'),
////                'id' => 'name',
////                'title' => __('Title'),
//                'class' => 'required-entry',
//
//            ]
//        );
//        $fieldset->addField(
//            'description',
//            'text',
//            [
//                'name' => 'description',
//                'label' => __('Description'),
////                'id' => 'name',
////                'title' => __('Title'),
//                'class' => 'required-entry',
//
//            ]
//        );
//        $fieldset->addField(
//            'rating',
//            'text',
//            [
//                'name' => 'rating',
//                'label' => __('Rating'),
////                'id' => 'name',
////                'title' => __('Title'),
//                'class' => 'required-entry',
//
//            ]
//        );
//        $fieldset->addField(
//            'director_id',
//            'text',
//            [
//                'name' => 'director_id',
//                'label' => __('Director_ID'),
////                'id' => 'name',
////                'title' => __('Title'),
//                'class' => 'required-entry',
//
//            ]
//        );
////        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);
//
////        $fieldset->addField(
////            'content',
////            'editor',
////            [
////                'name' => 'content',
////                'label' => __('Content'),
////                'style' => 'height:36em;',
////                'required' => true,
////                'config' => $wysiwygConfig
////            ]
////        );
//
////        $fieldset->addField(
////            'publish_date',
////            'date',
////            [
////                'name' => 'publish_date',
////                'label' => __('Publish Date'),
////                'date_format' => $dateFormat,
////                'time_format' => 'HH:mm:ss TT',
////                'class' => 'validate-date validate-date-range date-range-custom_theme-from',
////                'class' => 'required-entry',
////                'style' => 'width:200px',
////            ]
////        );
////        $fieldset->addField(
////            'is_active',
////            'select',
////            [
////                'name' => 'is_active',
////                'label' => __('Status'),
////                'id' => 'is_active',
////                'title' => __('Status'),
////                'values' => $this->_options->getOptionArray(),
////                'class' => 'status',
////                'required' => true,
////            ]
////        );
//        $form->setValues($model->getData());
//        $form->setUseContainer(true);
//        $this->setForm($form);
//
//        return parent::_prepareForm();
//    }
//}
