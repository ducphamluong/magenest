<?php

namespace Magenest\Movie\Block\Adminhtml\Movie;

use Magento\Backend\Block\Template;
use Magento\Sales\Model\Order\Creditmemo;

class Statistic extends Template
{
    protected $fullModuleList;
    protected $moduleList;
    protected $template;
//customer
    protected $customer;
    protected $product;
    protected $order;
    protected $invoice;
    protected $creditmemo;
    protected $moduleReader;

    /**
     * @param bool $isScopePrivate
     */
    public function setIsScopePrivate($isScopePrivate)
    {
        $this->_isScopePrivate = $isScopePrivate;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }
    public function __construct(
        Template\Context $context,
        \Magento\Framework\Module\FullModuleList $fullModuleList,
        \Magento\Framework\Module\ModuleList $moduleList,
        \Magento\Customer\Model\Customer $customers,
        \Magento\Catalog\Model\Product $product,
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Model\Order\Invoice $invoice,
        Creditmemo $creditmemo,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        array $data = [])
    {
        $this->fullModuleList = $fullModuleList;
        $this->moduleList = $moduleList;
        $this->customer = $customers;
        $this->product = $product;
        $this->order = $order;
        $this->invoice = $invoice;
        $this->creditmemo = $creditmemo;
        $this->moduleReader = $moduleReader;
        parent::__construct($context, $data);
    }
// code them chuc nang show list module in app/code
    public function getCustomModules()
    {
        $result = [];
        $modules = $this->moduleList->getNames();
        foreach ($modules as $_module) {
            $dir = $this->moduleReader->getModuleDir(null, $_module);
            if(strpos($dir, 'app/code') !== false)
            {
                $result[] = $_module;
            }
        }
        return $result;
    }
    public function getVendorModule()
    {
        //get list module not magento
        $allModules = $this->fullModuleList->getAll();
        $listOfModules = [];
        foreach ($allModules as $key => $value) {
            if(preg_match('/Magento_/', $key) == false)
             {
                $listOfModules[] = $value;
            }
        }
        return $listOfModules;

    }

    public function getAllModule()
    {
        //get list module magento
        $allModules = $this->fullModuleList->getAll();
        return $allModules;
    }

    public function getCustomerCollection()
    {
        return $this->customer->getCollection()
            ->addAttributeToSelect("*")
            ->load();
    }
    public function getProductCollection(){
        return $this->product->getCollection()
            ->addAttributeToSelect("*")
            ->load();
   }
    public function getOrderCollection(){
        return $this->order->getCollection()->getData();
    }
    public function getInvoiceCollection(){
        return $this->invoice->getCollection()
            ->addAttributeToSelect("*")
            ->load();
    }
    public function getCreditmemoCollection(){
        return $this->creditmemo->getCollection()
            ->addAttributeToSelect("*")
            ->load();
    }

}
