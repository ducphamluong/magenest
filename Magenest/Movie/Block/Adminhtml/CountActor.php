<?php

namespace Magenest\Movie\Block\Adminhtml;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class CountActor extends Field
{

    protected $_template = 'Magenest_Movie::system/config/count.phtml';

    protected $actor;
    public function __construct(
        Context $context,
        \Magenest\Movie\Model\Actor $actor,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->actor = $actor;
    }

    public function count(){
        $movieCollection = $this->actor->getCollection()->getData();
        return $movieCollection;
    }
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

}

