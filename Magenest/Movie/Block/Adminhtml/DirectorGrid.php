<?php
namespace Magenest\Movie\Block\Adminhtml;

class DirectorGrid extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        $this->_controller = 'adminhtml_driectorgrid';
        $this->_blockGroup = 'Magenest_Movie';
        $this->_headerText = __('Manage Director');
//        $this->_addButtonLabel = __('New Movie');
        parent::_construct();


//        $this->removeButton('add');
// for remove btn
//
        $this->buttonList->remove('add');
//        $this->buttonList->remove('back');
//        $this->buttonList->remove('reset');
        $this->buttonList->add(
            'add',
            [
                'label' => __('New Director'),
                'class' => 'primary',
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/addnew') . '\')'
            ]
        );


    }
//    protected function _prepareMassaction()
//    {
//        $this->setMassactionIdField('director_id');
//        $this->getMassactionBlock()->setFormFieldName('director_id');
//
//        $this->getMassactionBlock()->addItem(
//            'delete',
//            array(
//                'label' => __('Delete'),
//                'url' => $this->getUrl('director/*/massDelete'),
//                'confirm' => __('Are you sure?')
//            )
//        );
//        return $this;
//    }
}
