<?php
namespace Magenest\Movie\Block\Adminhtml;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Button extends Field
{
    protected function _getElementHtml(AbstractElement $element)
    {
        return '<button onclick="window.location.reload();" type="button">' . __('Reload') . '</button>';
    }
}
?>
